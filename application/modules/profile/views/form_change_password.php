<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <!-- <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div> -->
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Pengguna</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Username
     </div>
     <div class='col-md-5'>
      <input type='text' name='' id='username' class='form-control required' 
             value='<?php echo isset($username) ? $username : '' ?>' error="Username" disabled=""/>
     </div>     
    </div>
    <br/>   
    
    <div class="row">
     <div class='col-md-3'>
      Password Lama
     </div>
     <div class='col-md-5'>
      <input type='password' name='' id='password_lama' class='form-control required' 
             value='' error="Password Lama"/>
     </div>     
    </div>
    <br/>   
    
    <div class="row">
     <div class='col-md-3'>
      Password Baru
     </div>
     <div class='col-md-5'>
      <input type='password' name='' id='password_baru' class='form-control required'              
             value='' error="Password Baru"/>
     </div>     
    </div>
    <br/>   
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Template.simpanPassword()">Simpan</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
