
<div class="row">    
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box">
     <div class="box-header with-border">
      <h3 class="box-title"><?php echo ucfirst($title_content) ?></h3>
      <div class="divider"></div>
     </div>
     <!-- /.box-header -->
     <!-- form start -->
     <div class="box-body">
      <br/>
      <div class="row">
       <div class="col-md-12">
        <div class="input-group">
         <input type="text" class="form-control" id="keyword" placeholder="Pencarian">
         <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
       </div>
      </div>
      <br/>
      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered">
          <thead>
           <tr class="bg-primary-light text-white">
            <th>No</th>
            <th>Nama</th>
           </tr>
          </thead>
          <tbody>
           <tr>
            <td colspan="2">Tidak ada data ditemukan</td>
           </tr>
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>
    </div>

    <a href="#" class="float">
     <i class="fa fa-plus my-float fa-lg"></i>
    </a>
   </div>
  </div>       
 </div>
</div>