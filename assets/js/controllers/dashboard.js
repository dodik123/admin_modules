var Dashboard = {
 module: function () {
  return 'dashboard';
 },

 removeMenu: function (elm) {
  $(elm).closest('li').remove();
 },

 addLeftMenu: function (elm) {
  var left_menu = $(elm).closest('li');
  var newLeft = left_menu.clone();
  var input = '<div><input type="text" onkeyup="Dashboard.saveTitle(this, event)" class = "form-control required" error="Judul" placeholder="Isi Judul" /></div>';
  input += '<div class="text-right"><i class="fa fa-minus text-danger hover fa-lg" onclick="Dashboard.removeMenu(this)"></i></div>';
  newLeft.html(input);
  newLeft.css({
   padding: '16px'
  });
  left_menu.before(newLeft);
 },

 editLeftMenu: function (elm) {
  var room_title = $(elm).closest('li').attr('room_name');
  var room = $(elm).closest('li').attr('room');
  var id = $(elm).closest('li').attr('data_id');
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-left'>";
  html += "<h5>Masukkan Judul</h5>";
  html += "<div class='text-right'>";
  html += "<input type='text' class='form-control required' error='Judul' id='room_title_child' placeholder='Masukkan Judul' value='" + room_title + "'/><br/>";
  html += "<button data_id='" + id + "' parent_room='" + room + "' class='btn btn-success font-10'onclick='Dashboard.saveTitleEdit(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 editBodyDetail: function (elm, id) {
  var room_title = $(elm).closest('tr').attr('data_name');
  var id = $(elm).closest('tr').attr('data_id');
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-left'>";
  html += "<h5>Masukkan Judul</h5>";
  html += "<div class='text-right'>";
  html += "<input type='text' class='form-control required' error='Judul' id='room_title_child' placeholder='Masukkan Judul' value='" + room_title + "'/><br/>";
  html += "<button data_id='" + id + "' class='btn btn-success font-10'onclick='Dashboard.saveTitleEditBody(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 addRoom: function (nama = '', id = "") {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-left'>";
  html += "<h5>Masukkan Judul Room</h5>";
  html += "<div class='text-right'>";
  html += "<input type='text' class='form-control required' error='Judul' id='room_title' placeholder='Masukkan Judul' value='" + nama + "'/><br/>";
  html += "<button data_id='" + id + "' class='btn btn-success font-10'onclick='Dashboard.simpanRoom(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 deleteRoom: function (id, table) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button table='" + table + "' class='btn btn-success font-10'onclick='Dashboard.execDeleted(this," + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (elm, id) {
  var table = $(elm).attr('table');
  $.ajax({
   type: 'POST',
   data: {
    table: table
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Dashboard.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Dashboard.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addBody: function (elm, room_header) {
  $.ajax({
   type: 'POST',
   data: {
    room_header: room_header
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "addBody",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 editBody: function (elm, id) {
  $.ajax({
   type: 'POST',
   data: {
    id: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "editBody",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 simpanBody: function (elm, room_header) {
  if (validation.run()) {
   var title = $('input#title-body').val();
   var id = $('input#id_body').val();
   $.ajax({
    type: 'POST',
    data: {
     title: title,
     id: id,
     room_header: room_header
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Dashboard.module()) + "simpanBody",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Dashboard.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 simpanRoom: function (elm) {
  var id = $(elm).attr('data_id');
  if (validation.run()) {
   var title = $('input#room_title').val();
   $.ajax({
    type: 'POST',
    data: {
     title: title
     , id: id
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Dashboard.module()) + "simpanRoom",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Dashboard.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 saveTitle: function (elm, e) {
  if (e.keyCode == 13) {
   if (validation.run()) {
    var parent_room = $(elm).closest('div.row').attr('parent_room');
    var title = $(elm).val();
    $.ajax({
     type: 'POST',
     data: {
      title: title,
      room: parent_room
      , id: ''
     },
     dataType: 'json',
     async: false,
     url: url.base_url(Dashboard.module()) + "simpanTitle",
     error: function () {
      toastr.error("Gagal");
      message.closeLoading();
     },

     beforeSend: function () {
      message.loadingProses("Proses Simpan...");
     },

     success: function (resp) {
      if (resp.is_valid) {
       toastr.success("Berhasil Disimpan");
       var reload = function () {
        window.location.href = url.base_url(Dashboard.module()) + "index";
       };

       setTimeout(reload(), 1000);
      } else {
       toastr.error("Gagal Disimpan");
      }
      message.closeLoading();
     }
    });
   }
  }
 },

 saveTitleEdit: function (elm) {
  if (validation.run()) {
   var parent_room = $(elm).attr('parent_room');
   var title = $(elm).closest('div').find('input').val();
   var id = $(elm).attr('data_id');
   $.ajax({
    type: 'POST',
    data: {
     title: title,
     room: parent_room
     , id: id
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Dashboard.module()) + "simpanTitle",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Dashboard.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 saveTitleEditBody: function (elm) {
  if (validation.run()) {
   var title = $(elm).closest('div').find('input').val();
   var id = $(elm).attr('data_id');
   $.ajax({
    type: 'POST',
    data: {
     title: title
     , id: id
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Dashboard.module()) + "saveTitleEditBody",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Dashboard.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 detailBody: function (elm) {
  var id = $(elm).attr('data_id');
  var room = $(elm).attr('room');
  var room_name = $(elm).attr('room_name');
  $.ajax({
   type: 'POST',
   data: {
    room: room,
    room_name: room_name,
    id: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "detailBody",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    $('div#content-' + room).html(resp);
   }
  });
 },

 detail_trans: function (elm, id_body) {
  window.location.href = url.base_url(Dashboard.module()) + "detail_trans/" + id_body;
 },

 back: function (elm, id_body) {
  window.location.href = url.base_url(Dashboard.module()) + "index";
 },

 detailTrans: function (elm, id_body) {
  window.location.href = url.base_url(Dashboard.module()) + "detailTrans" + '/' + id_body;
 },

 changeTipeUpload: function (elm) {
  var tipe = $(elm).val();
  if (tipe == "no_upload") {
   $('div#div_upload').removeClass('hidden-content');
   $('div#div_upload').addClass('hidden-content');
  } else {
   $('div#div_upload').removeClass('hidden-content');
   if (tipe == "1") {
    tipe = "jpg";
    $('#file').attr('onchange', "Dashboard.checkFile(this, '" + tipe + "')");
   } else {
    tipe = "pdf";
    $('#file').attr('onchange', "Dashboard.checkFile(this, '" + tipe + "')");
   }
  }
 },

 getPostBodyData: function () {
  var data = {
   'room_body': $('input#room_body').val(),
   'title': $('input#judul').val(),
   'tipe_upload': $('select#tipe_upload').val(),
  };
  return data;
 },

 simpanTrans: function (id) {
  if (validation.run()) {
   var data = Dashboard.getPostBodyData();

   var formData = new FormData();
   formData.append('data', JSON.stringify(data));
   formData.append("id", id);
   formData.append('file', $('input#file').prop('files')[0]);

   if (validation.run()) {
    $.ajax({
     type: 'POST',
     data: formData,
     dataType: 'json',
     processData: false,
     contentType: false,
     async: false,
     url: url.base_url(Dashboard.module()) + "simpanTrans",
     error: function () {
      toastr.error("Gagal");
      message.closeLoading();
     },

     beforeSend: function () {
      message.loadingProses("Proses Simpan...");
     },

     success: function (resp) {
      if (resp.is_valid) {
       toastr.success("Berhasil Disimpan");
       var reload = function () {
        window.location.href = url.base_url(Dashboard.module()) + "detailTrans" + '/' + resp.id;
       };

       setTimeout(reload(), 1000);
      } else {
       toastr.error(resp.message);
      }
      message.closeLoading();
     }
    });
   }
  }
 },

 checkFile: function (elm, type) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    type = 'png';
   }
   if (type_file == 'jpeg') {
    type = 'jpeg';
   }
   if (type_file == type) {
    if (data_file.size <= 1324000) {
//     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
     console.log("boleh upload");
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat ' + type);
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showFile: function (elm, tipe) {
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).text()),
    tipe: tipe
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "showFile",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 showDetail: function (elm) {
  var id = $(elm).closest('tr').attr('data_id');
  $.ajax({
   type: 'POST',
   data: {
    room_body: id,
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "showDetail",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },
};


$(function () {

});
