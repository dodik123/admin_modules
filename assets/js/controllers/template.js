var Template = {
 moduleProfile: function () {
  return 'profile';
 },

 toggleMenu: function (elm, modul) {
  var modulClass = $('li.' + modul);
  modulClass.toggleClass('hide');
  modulClass.toggleClass('changeBgLeftContent');
  $(elm).toggleClass('changeBgLeftContent');
 },

 bodyOpen: function () {
  $('body').addClass('open');
 },

 changePassword: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Template.moduleProfile()) + "getFormChangePassword",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 simpanPassword: function () {
  var username = $('#username').val();
  var password_lama = $('#password_lama').val();
  var password_baru = $('#password_baru').val();

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: {
     username: username,
     password_lama: password_lama,
     password_baru: password_baru
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Template.moduleProfile()) + "simpanPassword",
    error: function () {
     message.closeLoading();
     toastr.error("Gagal Ubah Password");
    },

    beforeSend: function () {
     message.loadingProses("Proses Pergantian Password...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Password Telah Berhasil Diubah");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Ubah Password, " + resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 showUpdateSystem: function (elm, e) {
  e.preventDefault();
  var msg = "<p>Silakan Upgrade Sistem Anda..</p>";
  msg += "<p>Mohon segera kontak <b><a href='http://www.support@greenholetech.com'>support@greenholetech.com</a></b></p>";
  bootbox.dialog({
   message: msg,
   size: 'large'
  });
 },

 showDetailMenu: function (elm, e, name_class) {
  e.preventDefault();
  if (name_class != '') {
   $('nav.sidebar-nav').find('li').removeClass('active');
   var nav_menu = $('ul.' + name_class);
   var li_class = name_class.toString().replace('nav_', '');
   console.log(li_class);
   if (nav_menu.hasClass('hide')) {
    nav_menu.removeClass('hide');
    $('li.' + li_class).addClass('active');
   } else {
    nav_menu.addClass('hide');
    $('li.' + li_class).removeClass('active');
   }
  } else {
   window.location.href = $(elm).attr('href');
  }
 },

 setIsActiveMenu: function () {
  var module = window.location.href;
  module = module.split('/');
  var li_class = $.trim(module[4]);
  $('nav.sidebar-nav').find('li').removeClass('active');


  $('li.' + li_class).addClass('active');

//  $('ul');
 },

 setFixedLeftMenu: function () {
  var left = function () {
   $('aside.sidebar').css({
    position: 'fixed',
    overflow: 'hidden'
   });
  };

  setTimeout(left(), 1000);
 }
};

$(function () {
// Template.setFixedLeftMenu();
});